# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from gtfa import settings

import vk_api
import urllib, urllib2, os, string, zipfile, shutil

def zipfolder(foldername, target_dir):            
    zipobj = zipfile.ZipFile(foldername + '.zip', 'w', zipfile.ZIP_DEFLATED)
    rootlen = len(target_dir) + 2
    for base, dirs, files in os.walk(target_dir):
        for file in files:
            fn = os.path.join(base, file)
            zipobj.write(fn, fn[rootlen:])

def download_photos(tosave, folder, total):
    for url in tosave:
        file_name = url.split('/')[-1]
        u = urllib2.urlopen(url)
        f = open(settings.MEDIA_ROOT+'/'+folder+'/'+file_name, 'wb')
        meta = u.info()
        file_size = int(meta.getheaders("Content-Length")[0])
        print '[%s/%s] Downloading: %s Bytes: %s' % (tosave.index(url)+1, total, file_name, file_size)
        
        file_size_dl = 0
        block_sz = 8192
        while True:
            buffer = u.read(block_sz)
            if not buffer:
                break
            file_size_dl += len(buffer)
            f.write(buffer)
            status = r"%10d [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
            status = status + chr(8)*(len(status)+1)
            print status,
        f.close()

def main(request):
    
    def get_photos(owner_and_album):
        values = {
            'owner_id': owner_and_album[0],
            'album_id': owner_and_album[1],
        }
        tosave = []
        response = vk.method('photos.get', values)
        
        for i in response[u'items']:
        
            fin = False
            for r in resolutions:
                if not fin:
                    if r in i:
                        tosave.append(i[r])
                        fin = True
        return tosave
    
    def get_folder_name(owner_and_album):
        folder=''
        if owner_and_album[0][0] == '-':
            try:
                folder = folder + vk.method('groups.getById', {'group_id': owner_and_album[0][1:],})[0][u'name']
                #print 'public: '+folder
            except vk_api.api_error as error_msg:
                print(error_msg)
                errors.append(u'Ошибка во время получения названия группы: '+error_msg)
            
        else:
            try:
                user = vk.method('users.get', {'user_ids': owner_and_album[0],})[0]
                folder = folder + user[u'first_name'] + ' ' + user[u'last_name']
                #print 'user: '+folder
            except vk_api.api_error as error_msg:
                print(error_msg)
                errors.append(error_msg)
                #errors.append(u'Ошибка во время получения имени пользователя: '+error_msg)
        
        try:
            values = {
                'owner_id': owner_and_album[0],
                'album_ids': owner_and_album[1],
            }
            title = vk.method('photos.getAlbums', values)[u'items'][0][u'title']
            #print 'album: '+title
            folder = folder + ' # ' + title
        except vk_api.api_error as error_msg:
            print(error_msg)
            #errors.append(u'Ошибка во время получения названия альбома: '+error_msg)
            errors.append(error_msg)
        return folder
        
    
    login = u'+79612051253'
    password = u'leohueo228'
    number = None
    sid = None
    token = None
    proxies = None
    version = None
    app_id = 3916075
    errors = []
    tosave = []
    folder = ''
    query = ''
    
    ru_letters = u'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    valid_chars = '-_.(),+!@#$^& %s%s%s%s' % (string.letters, string.digits, ru_letters, ru_letters.upper() )
    resolutions = [u'photo_2560', u'photo_1280', u'photo_807', u'photo_604', u'photo_130', u'photo_75']
    
    if request.session.get('access_token'):
        token = request.session.get('access_token')
        print token
        login = None
        password = None
    
    try:
        vk = vk_api.VkApi(login, password, number, sid, token, proxies, version, app_id)
    except vk_api.authorization_error as error_msg:
        print(error_msg)
        errors.append(u'Ошибка во времся авторизации вконтакте:' + (error_msg))
    
    """try:
        wallpost = vk.method('wall.post', { 'owner_id': 2740015, 'message': u'Тест постинга в ВК якобы из standalone приложения', 'services':'facebook' })
    except vk_api.api_error as error_msg:
        print(error_msg)"""
    
    if request.POST.get('query'):
        query = request.POST.get('query')
        
        owner_and_album = query.split('album')[-1].split('_')
        if len(owner_and_album) == 2:
            try:
                tosave = get_photos(owner_and_album) 
            except vk_api.api_error as error_msg:
                print(error_msg)
                if error_msg[0][u'error_msg'] == u'Access denied':
                    return redirect('https://oauth.vk.com/authorize?client_id=3916075&scope=photos,groups,wall&response_type=token&redirect_uri=http://'+ request.get_host()+'/resp/')
            
            folder = get_folder_name(owner_and_album)
            total = str(len(tosave))
            print 'Total Files: ' + total
        else:
            errors.append(u'С вашей ссылкой что-то не так. Она должна быть вида: http://vk.com/album2740015_172028188 или album2740015_172028188 (если альбом находится в паблике то ссылки должны быть такими http://vk.com/album-39887252_158305983 или такими album-39887252_158305983)')
        
        if folder and total and tosave:
            try:
                folder = ''.join(c for c in folder if c in valid_chars)
                os.makedirs(settings.MEDIA_ROOT+'/'+folder+'')
            except OSError as exc:
                if exc.errno == '17':
                    errors.append(u'У нас произошла ошибка файловой системы. Попробуйте ещё раз или свяжитесь со мной.')
                print exc.errno
        
        download_photos(tosave, folder, total)

        
        print
        print 'everything is saved!'
        
        zipfolder(settings.MEDIA_ROOT+'/'+folder, settings.MEDIA_ROOT+'/'+folder+'/')
        
        try:
            shutil.rmtree(settings.MEDIA_ROOT+'/'+folder)
        except Exception:
            print 'directory not deleted'
        
    return render_to_response('main.html', {
        'errors':errors,
        'query':query,
    }, RequestContext(request))

def vk_response(request):
    
    if request.GET.get('access_token') and request.GET.get('user_id'):
        request.session['access_token'] = request.GET.get('access_token')
        request.session['user_id'] = request.GET.get('user_id')
        return redirect('/')
    
    return render_to_response('vk_response.html', {
    }, RequestContext(request))