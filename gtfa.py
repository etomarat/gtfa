# -*- coding: utf-8 -*-
print
print '  __          ___            _                                       '
print ' /__  _ _|_    | |_  o  _   |_     _ |  o ._     /\  | |_      ._ _  '
print ' \_| (/_ |_    | | | | _>   | |_| (_ |< | | |   /--\ | |_) |_| | | | '
print '                                                                     '
print
print 'connecting to the vk.com...'

import vk_api
import urllib, urllib2, os

def main():
    login = u'login@gmail.com'
    password = u'pass'

    res = [u'photo_2560', u'photo_1280', u'photo_807', u'photo_604', u'photo_130', u'photo_75']
    tosave = []
    folder = ''
    try:
        vk = vk_api.VkApi(login, password)
    except vk_api.authorization_error as error_msg:
        print(error_msg)  # В случае ошибки выведем сообщение
        return  # и выйдем
    
    print 'connecting complete'
    print
    url = raw_input('enter url?:')
    if url == '':
        raw_input('please enter the url! (press enter to exit)')
        return
    print
    
    owner_and_album = url.split('album')[-1].split('_')

    values = {
        'owner_id': owner_and_album[0],
        'album_id': owner_and_album[1],
    }
    response = vk.method('photos.get', values)
    
    if owner_and_album[0][0] == '-':
        try:
            folder = folder + vk.method('groups.getById', {'group_id': owner_and_album[0][1:],})[0][u'name']
        except Exception:
            pass
        print 'public: '+folder
    else:
        try:
            user = vk.method('users.get', {'user_ids': owner_and_album[0],})[0]
            folder = folder + user[u'first_name'] + ' ' + user[u'last_name']
        except Exception:
            pass
        print 'user: '+folder
    
    try:
        title = vk.method('photos.getAlbums', values)[u'items'][0][u'title']
        print 'album: '+title
        folder = folder + ' # ' + title
    except Exception:
        pass
    
    for i in response[u'items']:
        
        fin = False
        for r in res:
            if not fin:
                if r in i:
                    tosave.append(i[r])
                    fin = True
    
    total = str(len(tosave))
    
    print
    print 'Total Files: ' + total
    print
    
    try:
        os.makedirs(folder)
    except OSError:
        print 'OS Error!'
    
    for url in tosave:
        
        file_name = url.split('/')[-1]
        u = urllib2.urlopen(url)
        f = open(folder+'/'+file_name, 'wb')
        meta = u.info()
        file_size = int(meta.getheaders("Content-Length")[0])
        print '[%s/%s] Downloading: %s Bytes: %s' % (tosave.index(url)+1, total, file_name, file_size)
        
        file_size_dl = 0
        block_sz = 8192
        while True:
            buffer = u.read(block_sz)
            if not buffer:
                break
        
            file_size_dl += len(buffer)
            f.write(buffer)
            status = r"%10d [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
            status = status + chr(8)*(len(status)+1)
            print status,
    
        f.close()
        
    print
    print 'everything is saved!'
    
if __name__ == '__main__':
    main()